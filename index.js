"use strict";
/**
 * A tiny library to handle text translation.
 */

/**
 * init parameter object type
 * @typedef TranslatorParam
 * @property {String[]} supported_languages - DEFAULT: ["en"] - An array of ISO 639 lowercase language codes
 * @property {String} locale ISO 639 lowercase language code - DEFAULT: "en"
 *
 * @property {Boolean} use_url_locale_fragment - DEFAULT: true -
 * If true, the 1st fragment of the window.location url will be parsed as a language code
 * Example:
 * window.location
 * > https://example.com/en/some-page...
 * then the /en/ part of the url will be taken in priority and locale will be set to "en".
 * window.location
 * > https://example.com/some-page...
 * No locale fragment will be found so locale will not be set from url fragment.
 * window.location
 * > https://example.com/some-page/en
 * Doesn't work, locale fragment must be the first framgment
 *
 * @property {String} local_storage_key  - DEFAULT: "translator-prefered-language"
 *  The key used to saved the current locale into local storage
 *
 * @property {String} translations_url - REQUIRED - the url of the directory containing the static json files for translations
 * Translations files are expected to be named with their corresponding locale code.
 * Example:
 * if supported_languages is set to ["en", "fr", "it"]
 * and translations_url is set to "https://example.com/translations/""
 * Then the expected translations files are
 * https://example.com/translations/en.json
 * https://example.com/translations/fr.json
 * https://example.com/translations/it.json
 * The json resources must simple key value maps, value being the translated text.
 */

module.exports = {
    locale: "en", // ISO 639 lowercase language code
    supported_languages: ["en"],
    translations: {},
    translations_url: "",
    use_url_locale_fragment: true,
    local_storage_key: "translator-prefered-language",

    /**
     * Initialize the lib with params
     * @param {TranslatorParam} params 
     * @returns {Promise}
     */
    init(params) {
        Object.entries(params).forEach(k_v => {
            const [key, value] = k_v;
            if ([
                "supported_languages",
                "use_url_locale_fragment",
                "local_storage_key",
                "translations_url"
            ].includes(key)) {
                this[key] = value;
            }
        });

        this.translations_url = this.format_translations_url(this.translations_url);
        this.supported_languages = this.format_supported_languages(this.supported_languages);

        return new Promise((resolve, reject) => {
            const loc =
                (() => {// Locale from url priority 1
                    if (this.use_url_locale_fragment) {
                        const first_url_fragment = window.location.pathname.substring(1).split("/")[0];
                        const fragment_is_locale = this.supported_languages.includes(first_url_fragment);
                        return fragment_is_locale ? first_url_fragment : "";
                    } else {
                        return "";
                    }
                })()
                || localStorage.getItem(this.local_storage_key) // Locale from storage priority 2
                || (() => { // locale from navigator priority 3
                    const navigator_locale = navigator.language.split("-")[0].toLocaleLowerCase();
                    return this.supported_languages.includes(navigator_locale)
                        ? navigator_locale
                        : this.supported_languages[0] // Default if navigator locale is not supported
                })();

            fetch(`${this.translations_url}${loc}.json`)
                .then(response => response.json())
                .then(response => {
                    this.locale = loc;
                    this.translations = response;
                    resolve();
                })
                .catch(err => {
                    this.locale = "en";
                    reject(err);
                });
        });
    },

    /**
     * Return a lowercase string without dash.
     * If given locale is en-EN, then "en" will be returned.
     * @param {String} locale 
     * @returns A lowercase string
     */
    format_locale(locale) {
        return locale.split("-")[0].toLowerCase();
    },

    /**
     * Appends a slash at the end of the given string if missing, and returns the url.
     * @param {String} url 
     * @returns {String}
     */
    format_translations_url(url) {
        if (url.charAt(url.length - 1) !== "/") {
            url += "/"
        }
        return url;
    },

    /**
     * Return the array of language codes formatted as lowsercase language code.
     * if ["en-EN", "it-IT"]is given, ["en", "it"] will b returned.
     * @param {String[]} languages_codes 
     * @returns {String[]}
     */
    format_supported_languages(languages_codes) {
        return languages_codes.map(lc => this.format_locale(lc));
    },

    /**
     * Fetches a new set of translations in case the wanted language changes
     * @param {String} locale A lowercase language code
     * @returns {Promise}
     */
    update_translations(locale) {
        locale = this.format_locale(locale);
        return new Promise((resolve, reject) => {
            fetch(`${this.translations_url}${locale}.json`)
                .then(response => {
                    if (response.status === 200) {
                        return response.json();
                    } else {
                        throw Error("Translation file not found")
                    }
                })
                .then(response => {
                    this.translations = response;
                    this.locale = locale;

                    localStorage.setItem(this.local_storage_key, locale);

                    const split_path = window.location.pathname.substring(1).split("/");
                    const first_url_fragment = split_path[0];
                    const fragment_is_locale = this.supported_languages.includes(first_url_fragment);

                    if (fragment_is_locale) {
                        split_path.splice(0, 1, locale);
                        const updated_path = split_path.join("/");
                        window.history.replaceState(null, "", "/" + updated_path);
                    }
                    resolve();
                })
                .catch(err => {
                    reject(err);
                });
        });
    },

    /**
     * Tries to get the translation of the source string, or return the string as it.
     * @param {String} text The source text to translate
     * @param {Object} params Some dynamic element to insert in the text
     * Params can be used if the translated text provide placeholder like {%some_word%}
     * Example:
     * translator.trad("Some trad key", {some_word: "a dynamic parameter"})
     * -> translation for "Some trad key": "A translated text with {%some_word%}"
     * -> will be return as : "A translated text with a dynamic parameter"
     * @returns {String}
     */
    trad: function (text, params = {}) {
        text = this.translations[text] || text;

        Object.keys(params).forEach(k => {
            text = text.replace(`{%${k}%}`, params[k]);
        });

        return text;
    }
};