const { JSDOM } = require("jsdom");
const http = require("http");

const fr_json = {
    "hello": "bonjour",
    "You have X friends": "Tu as {%number%} amis"
};

const en_json = {
    "You have X friends": "You have {%number%} friends"
}

const es_json = {
    "hello": "hola",
    "You have X friends": "Tienes {%number%} amigos"

};

module.exports = function () {
    const base_html = `<!DOCTYPE html><html><body></body></html>`;
    global.DOM = new JSDOM(base_html);
    const { window } = DOM;
    global.window = window;

    const localStorageMock = (function () {
        let store = {}
        return {
            getItem: function (key) {
                return store[key] || null
            },
            setItem: function (key, value) {
                store[key] = value.toString()
            },
            clear: function () {
                store = {};
            }
        }
    })()

    Object.defineProperty(window, 'localStorage', {
        value: localStorageMock,
    });

    global.localStorage = window.localStorage;
    global.navigator = { language: "fr-FR" };
    global.SERVER_PORT = 8989;
    global.BASE_TEST_URL = "http://localhost" + ":" + SERVER_PORT;

    global.fetch = function (url) {
        return new Promise((resolve, reject) => {
            const req = new window.XMLHttpRequest();
            req.onload = function () {
                req.json = async function () {
                    const res = req.status === 200
                        ? JSON.parse(req.responseText)
                        : req.responseText;

                    return res;
                };
                resolve(req);
            };

            try {
                req.open("GET", BASE_TEST_URL + url);
                req.send();
            } catch (error) {
                reject(error)
            }
        });
    }

    const server = http.createServer((req, res) => {
        const path = req.url.replace(BASE_TEST_URL, "");
        res.setHeader("access-control-allow-origin", "*");
        res.setHeader("Content-Type", "application/json");

        switch (path) {
            case "/fr.json":
                res.end(JSON.stringify(fr_json));
                break;
            case "/en.json":
                res.end(JSON.stringify(en_json));
                break;
            case "/es.json":
                res.end(JSON.stringify(es_json));
                break;
            default:
                res.statusCode = 404;
                res.setHeader("Content-Type", "text/plain");
                res.end("File not found");
        }
    });

    const sockets = new Set();
    server.on('connection', (socket) => {
        sockets.add(socket);
        server.once('close', () => {
            sockets.delete(socket);
        });
    });

    global.close_server = () => {
        for (const socket of sockets) {
            socket.destroy(); sockets.delete(socket);
        }
        server.close();
    }

    server.listen(SERVER_PORT)
}