const createTestingContext = require("./create-testing-context");
const translator = require("..");

createTestingContext();

test("test trad fr", () => {
    navigator.language = "fr-FR";
    return translator.init({
        supported_languages: ["en", "fr", "es"],
        translations_url: "/",
        local_storage_key: "ls-test-trad",
    })
        .then(() => {
            expect(translator.trad("hello")).toBe("bonjour");
        });
});

test("test change trad", () => {
    return translator.update_translations("es").then(() => {
        expect(localStorage.getItem("ls-test-trad")).toBe("es");
        expect(translator.trad("hello")).toBe("hola");
    });
});

test("test trad params", () => {
    expect(translator.trad("You have X friends", { number: 0 }))
        .toBe("Tienes 0 amigos");
});

test("unsupported trad return key", () => {
    return translator.update_translations("it").catch(() => {
        expect(translator.trad("hello")).toBe("hola");
    });
});

test("unexisting key return key", () => {
    expect(translator.trad("whatever")).toBe("whatever");
});

test("use url locale fragment in priority", () => {
    expect(translator.locale).toBe("es");

    DOM.reconfigure({ url: "http://some-url.com/fr/some-path" });
    navigator.language = "it-IT";
    return translator.init({
        supported_languages: ["en", "fr", "es"],
        translations_url: "/",
    })
        .then(() => {
            expect(translator.locale).toBe("fr");
        });
});

test("ignore unsupported url locale fragment and get default", () => {
    expect(translator.locale).toBe("fr");

    DOM.reconfigure({ url: "http://some-url.com/de/some-path" });
    navigator.language = "it-IT";
    localStorage.clear();

    return translator.init({
        supported_languages: ["en", "fr", "es"],
        translations_url: "/",
    })
        .then(() => {
            expect(translator.locale).toBe("en");
        });
});

test("use local storage if defined", () => {
    expect(translator.locale).toBe("en");
    DOM.reconfigure({ url: "http://some-url.com/some-path" });// No locale fragment
    navigator.language = "it-IT"; // unsupported niavigator language

    localStorage.setItem(translator.local_storage_key, "fr");


    return translator.init({
        supported_languages: ["en", "fr", "es"],
        translations_url: "/",
    })
        .then(() => {
            close_server()
            expect(translator.locale).toBe("fr");
        });
});