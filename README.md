# KS-CHEAP-TRANSLATOR

**ks-cheap-translator is a very small and simple library to perform text translations on client side.**

## Usage

### Resources

ks-cheap-translator must be feed with json translations resources like :

```json
{
	"Hello translator": "Bonjour traducteur",
	"whatever-key-you-like.paragraph": "Some large translated text [...]"
	// ...
}
```

Translator will need an url to fetch the json resources as static json files, so you need to set up those file on your server side.

The the translator will be initialized with the list of your supported language and the url where your json translation files are.

The json files must be named from locale codes, if your supported language codes are `'it'`, `'en'`, `'es'`, then your json files will be expected to be named `it.json`, `en.json` and `es.json`.

---

### Initialization

```js
const translator = require("ks-cheap-translator");

translator
	.init({
		supported_languages: ["en", "it", "de", "fr"],
		use_url_locale_fragment: true,
		local_storage_key: "my-app-prefered-language",
		translations_url: "https://my-app-url/my-translations-dir/",
	})
	.then(() => {
		console.log("translations are ready !");
	})
	.catch(err => {
		console.log("Translation file was not found", err);
	});
```

-   **supported_languages** : An array of the lowsercase ISO 639 - the language codes corresponding to your supported translations.
-   **use_url_locale_fragment**: Boolean. Default is true - Wether the window.location url should be parsed to get the first fragment as a language code.

    **_Example_**: _if window.location is https://example.com/fr/some-path and fr is included in your supported languages then the /fr/ fragment will set the locale to fr automatically
    or if window.location is https://example.com/some-path
    url fragment is not used because /some-path/ is not a language code
    or if window.location is https://example.com/some-path/fr/
    /fr/ fragment will not be used because locale fragment is expected to be the first one._

-   **local_storage_key**: String - In order to make the prefered language setting persistent ks-cheap-translator uses a slot in the navigator local storage. Here you can set the key you want to be used for that. Default is "translator-prefered-language"

-   **translations_url**: String - REQUIRED - The url of the directory in which you store your translations json files.

    **_Example_**: _if your translation can be accessed with `https://example.com/static/translations/fr.json`, `https://example.com/static/translations/de.json`, etc, then you must give `https://example/static/translations/` as translations_url param._

---

### Translate

Text translation is performed simply by calling `translator.trad()` with the key of the text to translate.

```json
// fr.json
{
	"Some text to translate": "Du text à traduire"
}
```

```js
const translator = require("ks-cheap-translator");

const t = translator.trad.bind(translator);

let translated = t("Some text to translate");
console.log(translated);
// > "Du text à traduire";

// If the key is not found in translations, the key is returned as it
translated = t("A key you haven't set");
console.log(translated);
// > "A key you haven't set";
```

### Translation parameters

You can insert dynamic parameters in a translation:

```json
// fr.json
{
	"You have XXX new friends": "Vous avez {%friends_number%} nouveaux amis",
	"Your profile has been viewed * times since the ****": "Votre profil a été vu {%profile_views%} fois depuis le {%date%}"
}
```

```js
const translator = require("ks-cheap-translator");
const t = translator.bind(translator);

const friends_number = 12;
let translated = t("You have XXX new friends", { friends_number });
console.log(translated);
// > "Vous avez 12 nouveaux amis"

const profile_views = 23,
	date = "27/12/2021";
translated = t("Your profile has been viewed * times since the ****", {
	profile_views,
	date,
});
console.log(translated);
// > "Votre profil a été vu 23 fois depuis le 27/12/2021"
```

---

### Change language

At some point you will need to handle the case of a user changing language.

This is handled by calling the `update_translations` method.

```js
const translator = require("ks-cheap-translator");

function handle_language_change(new_locale) {
	translator
		.update_translations(new_locale)
		.then(() => {
			console.log("New translation resource is ready to be used");
		})
		.catch(err => {
			console.log(
				`Translations file was not found for locale ${new_locale}, ${translator.locale} will be kept.`,
			);
		});
}
```
